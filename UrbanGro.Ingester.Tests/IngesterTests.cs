using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using UrbanGro.Ingester;
using Moq;
using System.Collections.Generic;
using UrbanGro.Connectivity.MQTT;
using System.Text;
using System.Threading;
using static UrbanGro.Connectivity.MQTT.MqttClient;

namespace UrbanGro.Ingester.Tests
{
    [TestClass]
    public class IngesterTests
    {
        // For convenience of setting in the below methods that need valid external MQTT config
        public MqttConfig ValidMqttConfig = new MqttConfig()
        {
            Host = "mqtt.edyza.com",
            Port = 1885,
            Password = "edyza1",
            UserName = "peace-naturals-b3-rooma@urban-gro.com",
            Topic = "/output_42/#"
        };

        // Max number of seconds to wait for a message, in the tests that await
        public int MaxTimeToAwaitMessage = 10;

        /// <summary>
        /// Validates app doesn't crash when it can't reach the config service
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void TestConfigInit()
        {
            // Send a request that will not respond.  Exception should be caught and null returned.
            // There is nothing special about the URL, other than it being a port unlikely to be used
            AppConfig appConfig = Program.GetConfigRequest("https://localhost:4999/config").Result;
            Assert.IsNull(appConfig, "Failed to handle failed http request properly");
        }

        /// <summary>
        /// Validates parsing the incoming config json
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void TestConfigParse()
        {
            // Not-JSON parseable response
            string invalidJson = "{";
            Assert.IsNull(Program.ParseAppConfig(invalidJson), "App did not properly handle parsing invalid JSON");
        }
        
        /// <summary>
        /// Tests negative behavior around validating the incoming config
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void TestConfigValidate()
        {
            // Missing source config
            AppConfig missingSource = new AppConfig();
            RabbitMQConfig destConfig = new RabbitMQConfig()
            {
                Host = "localhost",
                Port = 1234,
                User = "user",
                Password = "pass",
                Exchange = "test_exchange",
                ExchangeType = "fanout",
                RoutingKey = ""
            };
            missingSource.AppDestinationConfig = destConfig;
            Assert.IsTrue(!Program.ValidateAppConfig(missingSource), "validateAppConfig did not correctly handle a missing source config");

            // Missing destination config
            AppConfig missingDest = new AppConfig();
            SourceConfig sourceConfig = new SourceConfig()
            {
                TenantId = "tenant",
                Host = "localhost",
                Port = 1234,
                Password = "pass",
                MqttTopics = new List<MqttTopicConfig>()
            };
            missingDest.AppSourceConfig = sourceConfig;
            Assert.IsTrue(!Program.ValidateAppConfig(missingSource), "validateAppConfig did not correctly handle a missing destination config");

            // Invalid exchange type
            AppConfig invalidExchange = new AppConfig();
            destConfig.ExchangeType = "invalid";
            invalidExchange.AppSourceConfig = sourceConfig;
            invalidExchange.AppDestinationConfig = destConfig;
            Assert.IsTrue(!Program.ValidateAppConfig(invalidExchange), "validateAppConfig did not correctly handle an invalid exchange type");
        }

        /// <summary>
        /// Tests expected behavior around Client_ConnectionClosed:
        ///   Method creates a new client and starts re-consuming
        ///   Disconnect counts and time are set
        /// </summary>
        [TestMethod]
        [TestCategory("Integration")]
        public void TestMqttClientReconnect()
        {
            DateTime testStart = DateTime.UtcNow;

            // Start MQTT client with junk receive handler and let it run a few seconds
            // We really need to establish a fake MQTT or local MQTT feed
            MqttClient client = new MqttClient(ValidMqttConfig);
            ManualResetEvent reset = new ManualResetEvent(false);
            client.MessageArrived += (sender, e) =>
            {
                // Let the thread continue since a message has arrived
                reset.Set();
            };
            client.Run();
            bool messageReceived = reset.WaitOne(TimeSpan.FromSeconds(MaxTimeToAwaitMessage));
            Assert.IsTrue(messageReceived, "Client not receiving messages initially");

            // Get a reference to the original client
            uPLibrary.Networking.M2Mqtt.MqttClient oldClient = client.Client;

            // Observe starting disconnect count and datetime
            Assert.AreEqual(0, client.DisconnectsCount, "Disconnect count at start not zero");
            Assert.IsNull(client.LastDisconnectTime, "Disconnect time at start not null");

            // Call the Client_ConnectionClosed and reset the manual reset event
            client.Client_ConnectionClosed(null, null);
            reset.Reset();

            // Observe disconnect count/time were set as expected
            Assert.AreEqual(1, client.DisconnectsCount, "Last disconnect count did not increment");
            Assert.IsNotNull(client.LastDisconnectTime, "Last disconnect datetime did not record");
            Assert.IsTrue(client.LastDisconnectTime > testStart, "Unexpected last disconnect datetime, should have been after the test start");
            
            // Observe a new client was instantiated
            Assert.AreNotEqual(oldClient, client.Client, "It does not appear a new client was established");

            // Observe there are messages receiving again
            messageReceived = reset.WaitOne(TimeSpan.FromSeconds(MaxTimeToAwaitMessage));
            Assert.AreNotEqual(client.MessageReceivedCount, 0, "Client not receiving messages after reset");

        }

        /// <summary>
        /// Tests that the client connected is being handled at correct times
        /// </summary>
        [TestMethod]
        [TestCategory("Integration")]
        public void TestMqttConnected()
        {
            // Client starts initialized
            MqttClient client = new MqttClient(ValidMqttConfig);
            Assert.AreEqual(client.ClientConnectionState, ClientState.Initialized, "MQTT client did not start disconnected");

            // Connect, client should be connected
            ManualResetEvent reset = new ManualResetEvent(false);
            client.MessageArrived += (sender, e) =>
            {
                // Do nothing
            };
            client.Run();
            Assert.AreEqual(client.ClientConnectionState, ClientState.Connected, "MQTT client not set to connected after started");

            // And stop the client normally, disconnected
            client.Stop();
            Assert.AreEqual(client.ClientConnectionState, ClientState.Disconnected, "MQTT client not set to disconnected after being stopped");

            // Check that a connected client, on becoming disconnected, sets the disconnect bool.
            client = new MqttClient(ValidMqttConfig);
            client.MessageArrived += (sender, e) =>
            {
                // Do nothing
            };
            ManualResetEvent disconnectReset = new ManualResetEvent(false);
            client.replaceDisconnectHandler((sender, e) =>
            {
                disconnectReset.Set();
            });
            client.Client_ConnectionClosed(client, null);
            bool disconnectHappened = disconnectReset.WaitOne(TimeSpan.FromSeconds(1));
            Assert.IsTrue(disconnectHappened, "MQTT disconnect code didn't call, sky is falling!");
            Assert.AreEqual(client.ClientConnectionState, ClientState.Disconnected, "MQTT client not set to disconnected after receiving a disconnect event");
        }

        /// <summary>
        /// Validates received messages that fail to invoke the MqttClient.Client_MqttMsgPublishReceived method are handled
        /// </summary>
        [TestMethod]
        [TestCategory("Integration")]
        public void TestMqttClientHandlesFailedPublish()
        {
            try
            {
                MqttClient client = new MqttClient(ValidMqttConfig);
                ManualResetEvent reset = new ManualResetEvent(false);
                client.MessageArrived += (sender, e) =>
                {
                    // We only really need to validate one message has arrived, it throws an exception,
                    // and the exception was caught
                    reset.Set();
                    throw new Exception("It all went wrong!");
                };
                client.Run();
                bool messageReceived = reset.WaitOne(TimeSpan.FromSeconds(MaxTimeToAwaitMessage));
                Assert.IsTrue(messageReceived, "No messages arrived to validate fail-publish");

            } catch (Exception)
            {
                Assert.Fail("Mqtt client did not catch thrown exception");
            }
            
        }

        /// <summary>
        /// This validates SecureEnabled in MqttClient manages the SslProtocols
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void TestSecurity()
        {
            MqttConfig testConfig = new MqttConfig()
            {
                Host = "host",
                Port = 1234,
                Password = "pass",
                UserName = "user",
                Topic = "topic"
            };
            // Verify default is secured
            Assert.IsTrue(testConfig.SecureEnabled);
            Assert.AreEqual(uPLibrary.Networking.M2Mqtt.MqttSslProtocols.TLSv1_2, testConfig.SslProtocol);

            // Verify unsetting and setting controls both
            testConfig.SecureEnabled = false;
            Assert.IsFalse(testConfig.SecureEnabled);
            Assert.AreEqual(uPLibrary.Networking.M2Mqtt.MqttSslProtocols.None, testConfig.SslProtocol);

            testConfig.SecureEnabled = true;
            Assert.IsTrue(testConfig.SecureEnabled);
            Assert.AreEqual(uPLibrary.Networking.M2Mqtt.MqttSslProtocols.TLSv1_2, testConfig.SslProtocol);

        }

        /// <summary>
        /// Validates MqttClient.Client_MqttMsgPublishReceived updates message receive counts/date
        /// </summary>
        [TestMethod]
        [TestCategory("Integration")]
        public void TestMqttSavesMessageCountsAndDates()
        {
            DateTime testStart = DateTime.UtcNow;
            MqttClient client = new MqttClient(ValidMqttConfig);

            // Observe starting message count and receive times
            Assert.AreEqual(0, client.DisconnectsCount, "Message receive count at start not zero");
            Assert.IsNull(client.LastDisconnectTime, "Message last received time at start not null");

            // Set a message receive event that will return quickly and start ingesting
            ManualResetEvent reset = new ManualResetEvent(false);
            client.MessageArrived += (sender, e) =>
            {
                // Let the thread continue since a message has arrived
                reset.Set();
            };
            client.Run();
            bool messageReceived = reset.WaitOne(TimeSpan.FromSeconds(MaxTimeToAwaitMessage));
            Assert.IsTrue(messageReceived, "No messages were ingested to be able to validate message count behavior");

            // Observe message count/time were incremented/set as expected
            Assert.IsTrue(client.MessageReceivedCount > 0, "Message received count did not increment");
            Assert.IsNotNull(client.LastMessageReceivedTime, "Message last received datetime did not record");
            Assert.IsTrue(client.LastMessageReceivedTime > testStart, "Unexpected last message received datetime, should have been after the test start");
                
        }

    }
}
