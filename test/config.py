
# The number of messages the test should receive before calling it a PASS
NUM_MESSAGES_PASS = 1

# Number of seconds to wait for the above number of messages to arrive
TIME_TO_AWAIT_MESSAGES = 30