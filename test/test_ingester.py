
# Functional test for ingester application

# Dependencies to run:
#    Edyza MQTT stream available
#    Local rabbitMQ service installed and running, with admin user test/test
#        see makefile target create-rabbit-test-user
#    (non docker) Python3.7 (because of datetime.fromisoformat) and pip.requirements.txt packages installed

from datetime import datetime
import dateutil.parser
import inspect
import json
import logging.config
import pika
import requests
import subprocess
import sys
import threading
import time
import uuid

import config

DEFAULT_LOGGING = { 
    'version': 1,
    'disable_existing_loggers': True, # Set this to false to see logging from other loggers, like pika
    'formatters': { 
        'standard': { 
            'format': '%(asctime)s [%(levelname)s] %(filename)s:%(lineno)d %(message)s'
        },
    },
    'handlers': { 
        'console': { 
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
    },
    'loggers': {  
        '': { # root logger
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True
        },
    } 
}

log = logging.getLogger()

# For printing convenience
message_count = 0

# Event to await a # of received messages
message_received_event = threading.Event()

# This is for benefit of allowing the test to continue, and report at the end
test_pass = True

# Other fields the receive thread will need to check
tenant_id = ""
mqtt_topics = []

# Main test method, do the stuff!
def test_ingester():

    log.info("Testing ingester")
    
    # Make sure we are (more or less) ready to go
    test_ready_check()
    
    # Cleanup any containers left behind by a failed test run
    app_list = ["ingester_app", "ingester_mock_service_app"]
    cleanup_apps(app_list)
    
    # Reset the local rabbitMQ environment
    test_queue_name = "ingester_test_queue"
    channel = reset_test_environment(test_queue_name)
    
    # Start the actual ingester application to test
    log.info("Starting up an ingester app")
    subprocess.run(['''docker run -d --name ingester_app --network="host" ingester_dist'''], check=True, shell=True)
    
    # Make sure the healthcheck API is working
    validate_healthcheck_api_at_start()
    
    # Wait a while before starting the mock service.  This is a flimsy validation of the 'poll forever for config'
    log.info("Waiting twenty seconds before starting the config mock service")
    time.sleep(20)
    
    # Start mock service and grab tenant ID and topics
    start_mock_service()
    
    # Start the PIKA client and validate all incoming messages
    validate_consumption(channel, test_queue_name)
    
    # Make sure the healthcheck API is correct after ingestion has started
    validate_healthcheck_api_after_consuming()
    
    # Stop the application so we can read its logs to validate the SIGTERM handler
    validate_sigterm()
    
    # Testing is done, lets do some quick cleanup
    cleanup_apps(app_list)

# Ensure test images are available locally, otherwise upon issuing `docker run` you sit 
# and wait for it to pull images from docker.io that don't exist,
def test_ready_check():
    
    log.info("Checking to make sure test images are available locally before starting the test")
    subprocess.run(["docker", "image", "inspect", "ingester_dist"],         check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    subprocess.run(["docker", "image", "inspect", "ingester_mock_service"], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

# Convenience code to cleanup existing apps.  Apps may or may not be present, hence why we don't check return code
def cleanup_apps(app_list):
    
    for app in app_list:
        destroy_docker_process(app, check_return_code=False)

# Kills and removes a docker container.  checkReturnCode controls if we care about the return code of the commands
# Kill is a bit faster, as the mock service has no SIGTERM handler
def destroy_docker_process(container_name, check_return_code=False):
    
    log.info("Killing " + container_name)
    subprocess.run(["docker", "kill", container_name], check=check_return_code)
    
    log.info("Removing " + container_name)
    subprocess.run(["docker", "rm", container_name], check=check_return_code)

# Connects to local rabbitMQ, deletes the test exchange/queue, recreates them, and binds them.  Returns the channel needed by the consumer
def reset_test_environment(test_queue_name):
    
    log.info("Resetting test environment")
    
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    
    log.info("Deleting existing exchange (if any)")
    test_exchange_name = "ingester_test_exchange"
    channel.exchange_delete(exchange=test_exchange_name)
    
    log.info("Deleting existing queue (if any)")
    channel.queue_delete(queue=test_queue_name)
    
    log.info("Creating a new queue")
    channel.queue_declare(queue=test_queue_name, durable=True)
    
    log.info("Creating a new exchange")
    channel.exchange_declare(exchange=test_exchange_name, exchange_type="fanout", durable=True)
    
    log.info("Binding exchange to queue")
    channel.queue_bind(queue=test_queue_name, exchange=test_exchange_name)
    
    log.info("Environment is reset!")
    return channel

# This will start the application mock service and retrieve values needed to test against
def start_mock_service():
    
    # Start the mock config service container. We attach networking here so that the mock service has the localhost IP to bind to.
    # Docker command line didn't like passing in the --network="host" using argument list, hence the long string + shell=True
    log.info("Starting up a mock config service with ingester_mock_service")
    subprocess.run(['''docker run -d --name ingester_mock_service_app --network="host" ingester_mock_service'''], check=True, shell=True)
    
    # Give it a sec to start up
    log.info("Waiting a smidge for the mock service to start")
    time.sleep(3)
    
    # Get the tenant ID and topics from the mock config, as we need those to test against ingester output
    global tenant_id
    global mqtt_topics
    
    log.info("Getting config from mock service")
    response_json = requests.get("https://localhost:5000/config", verify=False).json()
    tenant_id = response_json["source"]["tenant_id"]    
    for topic in response_json["source"]["mqtt_topics"]:
        
        # Strip out wildcard characters
        topic_string = topic["topic"].replace("#", "").replace("+", "")
        mqtt_topics.append(topic_string)

# Starts the message consumption, waits, and validates messages were received
def validate_consumption(channel, test_queue_name):
        
    # Start a separate thread to run the consumer.  The daemon=True lets python kill the thread when there are only daemon threads remaining
    pika_thread = PikaConsumer(daemon=True, kwargs={"channel": channel, "queue": test_queue_name})
    pika_thread.start()
    
    # Let 'er rip, until the message_received_event timeout
    global message_received_event
    global message_count
    log.info("Consuming messages until " + str(config.NUM_MESSAGES_PASS) + " messages are received, or a " + str(config.TIME_TO_AWAIT_MESSAGES) + " second timeout" )
    messages_received = message_received_event.wait(timeout=config.TIME_TO_AWAIT_MESSAGES)
    expect(messages_received, "There were not enough messages ingested (" + str(message_count) + " messages), check your MQTT source")
    
    # Just a final convenience print, in case we got fewer than expected messages
    log.info(str(message_count) + " messages were consumed")

# This is intended to work similar to google test's EXPECT keyword, in that it will cause a test failure, without halting test execution.
# For it to work, a failing assertion on global->test_pass must be called before exiting the program.
def expect(conditional, message):
    
    # Some basic checks so method isn't called inappropriately.
    if type(conditional) is not bool:
        assert False, "The first 'expect' argument must be an evaluatable condition"
    if type(message) is not str:
        assert False, "The second 'expect' argument must be a string"
    
    # Handle fail evaluation
    if not conditional:
        
        # Test is a fail
        global test_pass
        test_pass = False
        
        # Grab the calling stack so we can print in the log message
        # stack() returns list of FrameInfo(frame, filename, lineno, function, code_context, index)
        # and 1 is the stack level immediately above this, hence the [1][0]
        calling_frame = inspect.stack()[1][0]
        frame_info = inspect.getframeinfo(calling_frame)
        log.error("[EXPECT " + frame_info.filename + ":" + str(frame_info.lineno) + "] " + message)
            
# Checks that the healthcheck API is running and of expected format when the application starts
def validate_healthcheck_api_at_start():
    
    log.info("Sleeping a couple seconds to give the application http server time to start")
    time.sleep(2)
    log.info("Testing /health API")
    
    # Make the request, it should respond with status code 200
    response = requests.get("http://localhost:8000/health")
    expect(response.status_code == 200, "Unexpected healthcheck return code: " + str(response.status_code))
        
    # Parse the response and validate fields are present
    response_json = response.json()
    expect(response_json.get("healthy") in [True,False], "'healthy' attribute missing or not a boolean value: '" + str(response_json.get("healthy")) + "'")
    expect(response_json.get("message") == "Awaiting configuration", "'message' attribute missing or not currently set to 'Awaiting configuration': '" + str(response_json.get("message")) + "'")
    expect(response_json.get("applicationName") == "Ingester", "'applicationName' attribute missing or not set to 'Ingester': '" + str(response_json.get("applicationName")) + "'")
    expect(response_json.get("version") is not None, "'version' attribute missing or is set to null: '" + str(response_json.get("version")) + "'")
    expect(response_json.get("details") is not None 
           and response_json["details"].get("ingester_feeds") is not None
           and type(response_json["details"]["ingester_feeds"]) is list,
           "'details.ingester_feeds' attributes missing or not appearing as expected: '" + str(response_json.get("details")) + "'")
    
# Checks fields appearing in the API after consumption is started
def validate_healthcheck_api_after_consuming():
    
    log.info("Testing /health API after consuming")
    
    response = requests.get("http://localhost:8000/health")
    response.raise_for_status()
    response_json = response.json()
    
    # Check the message is set to running
    expect(response_json.get("message") == "Running", "'message' attribute is not currently set to 'Running': '" + str(response_json.get("message")) + "'")
    
    global mqtt_topics
    # Check the individual feed objects
    for feed in response_json["details"]["ingester_feeds"]:
        
        # Check all values are present
        # The topic should correspond to one of the config tpics
        expect(feed.get("mqtt_topic") in mqtt_topics, "'mqtt_topic' attribute missing or not in the source config list: '" + str(feed.get("mqtt_topic")) + "'")
        expect(feed.get("mqtt_messages_received") >= 0, "'mqtt_messages_received' attribute missing or not an integer: '" + str(feed.get("mqtt_messages_received")) + "'")
        expect("mqtt_last_message_received" in feed, "'mqtt_last_message_received' attribute missing")
        expect(feed.get("mqtt_disconnects") >= 0, "'mqtt_disconnects' attribute missing or not an integer: '" + str(feed.get("mqtt_disconnects")) + "'")
        expect("mqtt_last_disconnect" in feed, "'mqtt_last_disconnect' attribute missing")
        expect(feed.get("rabbit_messages_published") >= 0, "'rabbit_messages_published' attribute missing or not an integer: '" + str(feed.get("rabbit_messages_published")) + "'")
        expect("rabbit_last_message_published" in feed, "'rabbit_last_message_published' attribute missing")
                
        # After consuming, the last received/published should be parseable datetimes
        if feed.get("mqtt_messages_received") is not None and feed.get("mqtt_last_message_received") is not None:
            expect(type(parse_iso8601_date(feed["mqtt_last_message_received"])) is datetime, 
                   "Failed to parse date for field 'mqtt_last_message_received': '" + str(feed.get("mqtt_last_message_received")) + "'")
        if feed.get("rabbit_messages_published") is not None and feed.get("rabbit_last_message_published") is not None:
            expect(type(parse_iso8601_date(feed["rabbit_last_message_published"])) is datetime, 
                   "Failed to parse date for field 'rabbit_last_message_published': '" + str(feed.get("rabbit_last_message_published")) + "'")

# Just a helper method, so that exceptions thrown by dateutil.parser.parse don't break the program
def parse_iso8601_date(string_date):
    
    retval = None
    try:
        retval = dateutil.parser.parse(string_date)
    except (ValueError, OverflowError) as ex:
        pass
    return retval
    
# We need a separate class to initialize and start_consuming, so that we can timeout from the main thread
class PikaConsumer(threading.Thread):

    # Init to collect args
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, daemon=None):
        
        super(PikaConsumer, self).__init__(group=group, target=target, name=name, daemon=daemon)
        self.args = args
        self.kwargs = kwargs

    # Override of Thread.run, launches the channel.start_consuming and runs until program ends
    def run(self):
        
        try:
            
            # Set up channel for consumption
            log.info("Setting up consumption behavior")
            channel = self.kwargs["channel"]
            channel.basic_consume(message_received_callback, queue=self.kwargs["queue"], no_ack=True)
            
            # And begin consuming
            log.info("Beginning consumption")
            channel.start_consuming()
        
        # Uncaught thread exceptions will still print to stdout, however this ensures logging intercepts it (for non stdout logging)
        except Exception as ex:

            log.exception(ex)

# basic_consume callback to receive and validate messages
def message_received_callback(channel, method, properties, body):
    
    # Main thread variables we need to read and/or pass upward        
    global message_count
    global message_received_event
    global tenant_id
    global mqtt_topics
    
    message_count += 1
    if message_count % 10 == 0:
        log.info("Received " + str(message_count) + " messages")
    
    # Parse the message, it should always be JSON
    try:
        jsonBody = json.loads(body)
    except json.JSONDecodeError:
        expect(False, "Message received was not JSON: " + str(body))
        return
    
    # This allows us to expose more issues with message before leaving
    bad_message = False
    
    # There should be a msgId at top level 
    if jsonBody.get("msgId") is None:
        log.error("Message did not contain a msgId")
        bad_message = True
    else:
        # and it should be a GUID
        try:
            uuid.UUID(jsonBody.get("msgId"))
        except ValueError:
            log.error("Message had a non-guid msgId")
            bad_message = True
    
    # There should be a tenantId
    if jsonBody.get("tenantId") is None:
        log.error("Message did not contain a tenantId")
        bad_message = True
    else:
        # and it should match the tenant ID from the mock service config
        if jsonBody.get("tenantId") != tenant_id:
            log.error("Config defined tenant ID not present in message")
            bad_message = True
        
    # There should be a mqttTopic field
    if jsonBody.get("mqttTopic") is None:
        log.error("Message did not contain an mqttTopic")
        bad_message = True
    else:
        
        # and it should be in the list of mock source config topics (that we removed wildcards from)
        test_topic = jsonBody.get("mqttTopic")
        topic_in_list = False
        for source_topic in mqtt_topics:
            if source_topic in test_topic:
                topic_in_list = True
                break
        if not topic_in_list:
            log.error("Unexpected incoming topic " + test_topic)
            log.info("topics list " + str(mqtt_topics))
            bad_message = True
            
        # Additionally, preservation and psychrometric readings should have been dropped by ingester
        for bad_topic in ["preservation", "psychrometric", "hvac", "air_flow"]:
            if bad_topic in test_topic:
                log.error("Topic that was expected to drop found incoming " + test_topic)
                bad_message = True
    
    # There should be a rawMsg field.
    if jsonBody.get("rawMsg") is None:
        log.error("Message did not contain a rawMsg")
        bad_message = True
    else:
        # and it should have content, that is, at a minimum, an empty JSON object {}
        if type(jsonBody.get("rawMsg")) is not dict:
            log.info("Raw message was not a JSON object")
            bad_message = True
    
    # Inform when the message was bad
    expect(not bad_message, "Bad message content: " + str(jsonBody))

    # If we've received enough messages, set the event for test listener
    if message_count >= config.NUM_MESSAGES_PASS:
        message_received_event.set()

# Stops ingester application and validates the SIGTERM ran
def validate_sigterm():
    
    log.info("Stopping ingester to validate SIGTERM")
    subprocess.run(["docker", "stop", "ingester_app"], check=True)
    completed_process = subprocess.run(["docker", "logs", "ingester_app"], check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    expect("Application has shut down" in completed_process.stdout.decode("utf-8"), "SIGTERM handler did not appear to execute")

# Entry point
if __name__ == "__main__":
    
    # Generic try/except at top level, to catch anything uncaught in the main thread
    try:
        logging.config.dictConfig(DEFAULT_LOGGING)
        test_ingester()
        
        # Fail the test for any EXPECT-failures
        assert test_pass, "There was a test failure at some point, check the logs"
        log.info("Ingester passed all tests!")
    
    # Anything goes wrong, its a test fail
    except Exception as ex:
        log.exception(ex)
        sys.exit(1)

