from flask import Flask, jsonify
import json
import sys

app = Flask(__name__)

# Separate configs allow us to substitute values faster

# The edyza feed.  This is our current go-to for real data and volume, and the local tests
source_edyza = {
            "tenant_id": "DA85C77A-C5CB-40F7-A66A-0F9D3684DCD6",
            "host": "mqtt.edyza.com",
            "port": 1885,
            "password": "edyza1",
            "mqtt_topics": [
                {"user": "peace-naturals-b3-rooma@urban-gro.com", "topic": "/output_42/#"},
                {"user": "peace-naturals-b3-roomb@urban-gro.com", "topic": "/output_43/#"},
                {"user": "peace-naturals-b3-roomc@urban-gro.com", "topic": "/output_44/#"},
                {"user": "peace-naturals-b3-roomd@urban-gro.com", "topic": "/output_45/#"},
                {"user": "peace-naturals-b3-roome@urban-gro.com", "topic": "/output_46/#"},
                {"user": "peace-naturals-b3-roomf@urban-gro.com", "topic": "/output_47/#"},
                {"user": "peace-naturals-b3-roomg@urban-gro.com", "topic": "/output_48/#"},
                {"user": "peace-naturals-b3-roomi@urban-gro.com", "topic": "/output_49/#"}
            ],
            "dropped_connection_retry_secs": 5
        }

# This is the local lab feed, it does not use SSL/TLS
source_the_lab = {
            "tenant_id": "DA85C77A-C5CB-40F7-A66A-0F9D3684DCD6",
            "host": "10.62.120.214",
            "port": 1883,
            "password": "urbangro1",
            "mqtt_topics": [
                {"user": "soleil", "topic": "soleil/sensor_data/feed_a"},
            ],
            "dropped_connection_retry_secs": 5,
            "secure_enabled": False
        }

# Localhost rabbit destionation.  This should be used along with source_edyza for the local test run
destination_local = {
        
            "host": "localhost",
            "port": 5672,
            "user": "test",
            "password": "test",
            "exchange": "ingester_test_exchange",
            "exchange_type": "fanout",
            "routing_key": "ingester_test_queue"
        }

# For development substitution
destination_dev = {
        
            "host": "10.62.120.223",
            "port": 5672,
            "user": "test",
            "password": "test",
            "exchange": "ingester_test_exchange",
            "exchange_type": "fanout",
            "routing_key": "ingester_test_queue"
        }

# The config JSON this will respond with, setup so we can switch stuff on the fly.
# This should use source_edyza/source_the_lab and destination_local for a local test run
config_json = {
        
        "source": source_the_lab,
        "destination": destination_local
    }

@app.route('/config')
def config():
    
    return jsonify(config_json)

if __name__ == "__main__":
    
    # Allow first argument to override the destination
    print("args len " + str(len(sys.argv)))
    if len(sys.argv) > 1:
        config_json["destination"] = json.loads(sys.argv[1])
    
    # This is super-cheap, unsigned https, as we wouldn't want to transmit passwords over http
    app.run('0.0.0.0', ssl_context='adhoc')
    
    
    
    
    
    
    