﻿using System;
using System.Collections.Generic;
using System.Text;
using uPLibrary.Networking.M2Mqtt;

namespace UrbanGro.Connectivity.MQTT
{
    public class MqttConfig
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Topic { get; set; }

        /// <summary>
        /// The number of seconds to wait between reconnect attempts when disconnected.
        /// Defaulted to ten seconds, so it doesn't try to reconnect with no delay
        /// </summary>
        public int ReconnectSeconds { get; set; } = 10;

        /// <summary>
        /// Whether to use secure + TLSv1.2 or not.  Currently only our lab DEV feed should have this disabled
        /// </summary>
        private bool _secureEnabled = true;
        public bool SecureEnabled
        {
            get
            {
                return _secureEnabled;
            }
            set
            {
                if (value)
                {
                    _secureEnabled = true;
                    _sslProtocol = MqttSslProtocols.TLSv1_2;
                } else
                {
                    _secureEnabled = false;
                    _sslProtocol = MqttSslProtocols.None;
                }
            }
        }

        /// <summary>
        /// The SSL protocol; default is TLSv1.2.  SecureEnabled currently drives whether this is set
        /// </summary>
        private MqttSslProtocols _sslProtocol = MqttSslProtocols.TLSv1_2;
        public MqttSslProtocols SslProtocol
        {
            get
            {
                return _sslProtocol;
            }
        }


    }
}
