﻿using System;
using System.Collections.Generic;
using System.Text;
using UrbanGro.Connectivity.RabbitMQ;
using UrbanGro.Ingester;

namespace UrbanGro.Connectivity.MQTT
{
    public class MessageEventArgs : EventArgs
    {
        public byte[] Message { get; set; }
        public string Topic { get; set; }
        public bool IsRetained { get; set; }
    }
}