﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using uPLibrary.Networking.M2Mqtt.Messages;
using UrbanGro.Ingester;

namespace UrbanGro.Connectivity.MQTT
{
    public class MqttClient
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MqttClient));

        public uPLibrary.Networking.M2Mqtt.MqttClient Client { get; private set; }
        public MqttConfig Config { get; }
        public enum ClientState { Initialized, Connected, Disconnected };
        public ClientState ClientConnectionState { get; set; }

        public string ClientId { get; private set; }
        public event EventHandler<MessageEventArgs> MessageArrived = null;
        public event EventHandler ClientDisconnected = null;

        public long MessageReceivedCount { get; set; } = 0;
        public DateTime? LastMessageReceivedTime { get; set; } = null;

        /// <summary>
        /// Number of disconnects.  It is important to track these, as the disconnect handle event is already a hackaround for the library
        /// not reconnecting as advertised.  If the disconnects are happening too often, we need to know
        /// </summary>
        public int DisconnectsCount { get; set; } = 0;
        public DateTime? LastDisconnectTime { get; set; } = null;
  
        /// <summary>
        /// Creates a new client and adds its receive/disconnect event handlers
        /// </summary>
        /// <param name="config">The client config</param>
        /// <param name="extraArgs">An optional dictionary of key/values that should additionally accompany returned MessageEventArgs</param>
        public MqttClient(MqttConfig config)
        {
            ClientId = Guid.NewGuid().ToString();
            Config = config ?? throw new ArgumentNullException(nameof(config));
            ClientConnectionState = ClientState.Initialized;
            EstablishClient();
        }

        /// <summary>
        /// Establish the client object, and add its event handlers, not yet connected
        /// </summary>
        private void EstablishClient()
        {
            Client = new uPLibrary.Networking.M2Mqtt.MqttClient(Config.Host, Config.Port, Config.SecureEnabled, null, null, Config.SslProtocol);
            Client.ConnectionClosed += Client_ConnectionClosed;
            Client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
            ClientDisconnected += Client_Reconnect;
        }

        /// <summary>
        /// Convenience method to remove the event handlers
        /// </summary>
        private void RemoveEventHandlers()
        {
            ClientDisconnected = null;
            Client.MqttMsgPublishReceived -= Client_MqttMsgPublishReceived;
            Client.ConnectionClosed -= Client_ConnectionClosed;
        }
        
        /// <summary>
        /// This allows replacement of the disconnect handler.  It is only used by the tests, and should not be called by anything else.
        /// </summary>
        /// <param name="replacementHandler"></param>
        internal void replaceDisconnectHandler(EventHandler replacementHandler)
        {
            ClientDisconnected -= Client_Reconnect;
            ClientDisconnected += replacementHandler;
        }

        /// <summary>
        /// Connects and subscribes, once instantiated
        /// </summary>
        public void Run()
        {
            Client.Connect(ClientId, Config.UserName, Config.Password);
            Client.Subscribe(new[] { Config.Topic }, new[] { (byte)MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE });
            ClientConnectionState = ClientState.Connected;
        }

        /// <summary>
        /// Shuts down the client and removes its event handlers
        /// </summary>
        public void Stop()
        {
            if (ClientConnectionState == ClientState.Initialized)
                return;

            try
            {
                Client.Disconnect();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            RemoveEventHandlers();
            ClientConnectionState = ClientState.Disconnected;
        }

        /// <summary>
        /// Handle receiving a message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Client_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            try
            {
                // Increment the message receive count and save the time it happened
                MessageReceivedCount++;
                LastMessageReceivedTime = DateTime.UtcNow;

                MessageArrived?.Invoke(this, new MessageEventArgs
                {
                    IsRetained = e.Retain,
                    Message = e.Message,
                    Topic = e.Topic
                });
                
            }
            catch (Exception ex)
            {
                log.Error("Failed to invoke message process method:");
                log.Error(ex);
            }
        }

        /// <summary>
        /// Handle disconnects
        /// This is to deal with the fact that this library doesn't seem to handle disconnections as advertised.
        /// EventArgs for Connection Closed is 'EventArgs.EMPTY'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Client_ConnectionClosed(object sender, EventArgs e)
        {

            try
            {
                ClientConnectionState = ClientState.Disconnected;
                log.Info("Received a connection closed message, trying to reconnect");

                // Update our disconnect info, because information is good
                DisconnectsCount++;
                LastDisconnectTime = DateTime.UtcNow;

                ClientDisconnected?.Invoke(this, null);


            } catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        // Handle client reconnect
        public static void Client_Reconnect(object sender, EventArgs e)
        {
            try
            {
                MqttClient client = (MqttClient)sender;

                while (client.ClientConnectionState == ClientState.Disconnected)
                {
                    try
                    {
                        log.Info("Destroying previous receive handlers");
                        client.RemoveEventHandlers();

                        log.Info("Creating a new client");
                        client.EstablishClient();

                        log.Info("Starting client");
                        client.Run();

                    }
                    catch (MqttConnectionException reconnectEx)
                    {
                        log.Error("Failed to reconnect, better luck next time.  Sleeping " + client.Config.ReconnectSeconds.ToString() + " seconds", reconnectEx);
                        Thread.Sleep(client.Config.ReconnectSeconds * 1000);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

    }
}