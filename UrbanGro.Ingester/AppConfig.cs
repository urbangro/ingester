﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace UrbanGro.Ingester
{
    public class AppConfig
    {
        [JsonProperty("source")]
        public SourceConfig AppSourceConfig { get; set; }

        [JsonProperty("destination")]
        public RabbitMQConfig AppDestinationConfig { get; set; }
    }

    public class SourceConfig
    {
        [JsonProperty("tenant_id")]
        public string TenantId { get; set; }

        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("mqtt_topics")]
        public IList<MqttTopicConfig> MqttTopics { get; set; }
        
        [JsonProperty("dropped_connection_retry_secs")]
        public int DroppedConnectionRetrySeconds { get; set; }

        /// <summary>
        /// Whether the connection is secured or not, defaulted to true
        /// </summary>
        [JsonProperty("secure_enabled")]
        public bool SecureEnabled { get; set; } = true;
    }

    public class MqttTopicConfig
    {
        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("topic")]
        public string Topic { get; set; }
    }
    
    public class RabbitMQConfig
    {
        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("exchange")]
        public string Exchange { get; set; }

        public enum AllowableExchangeTypes { direct, fanout, headers, topic };
        [JsonProperty("exchange_type")]
        public string ExchangeType { get; set; }

        [JsonProperty("routing_key")]
        public string RoutingKey { get; set; }
    }

}
