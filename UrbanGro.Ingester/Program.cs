﻿using log4net;
using log4net.Config;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UrbanGro.Connectivity.MQTT;
using UrbanGro.Connectivity.RabbitMQ;
using RabbitMQ.Client;
using UrbanGro.Utilities.Web;
using static UrbanGro.Connectivity.MQTT.MqttClient;

namespace UrbanGro.Ingester
{
    public class Program
    {        
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        private static AppConfig _appConfig = null;
        private static ConcurrentBag<MqttClient> _clients = new ConcurrentBag<MqttClient>();
        private static Dictionary<MqttClient, Publisher> _publishers = new Dictionary<MqttClient, Publisher>();
        private static IConnection _rabbitConnection = null;
        private static HealthCheckListener _healthCheckListener = null;
        
        // Args[0] is the URL of the config service discovery, else it is read in @ runtime
        static void Main(string[] args)
        {
            
            // Application catch-all try/catch
            try
            { 
                // Application unhandled exception handler.  This differs from the above catch-all as it will catch thread-thrown exceptions
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                // Configure application logging
                ConfigureLogging();

                // Have application shutdown for CTRL-C, for developer benefit
                Console.CancelKeyPress += delegate {
                    AppShutdown();
                };
                // Have application shutdown on SIGTERM, for `docker stop` benefit
                AppDomain.CurrentDomain.ProcessExit += (s, e) =>
                {
                    AppShutdown();
                };

                // Create and start a health check listener.  It is done early as reasonably possible
                // Leaving this unparameterized for the moment, as docker can manage IP/port mappings
                log.Info("Starting health check listener");
                _healthCheckListener = new HealthCheckListener("0.0.0.0", 8000);
                _healthCheckListener.HealthRequested += Listener_HealthRequested;
                _healthCheckListener.Start();

                // Get the application configuration
                _appConfig = GetConfiguration(args);
                
                // Create RabbitMQ connection.  We will add publishers for each MQTT client
                IConnectionManager rabbitConnectionManager = new ConnectionManager();
                _rabbitConnection = rabbitConnectionManager.CreateConnection(_appConfig.AppDestinationConfig.Host, _appConfig.AppDestinationConfig.User, _appConfig.AppDestinationConfig.Password);
                
                // Establish mqtt clients configs from incoming config
                List<MqttConfig> sourceConfigs = CreateSourceConfigs();

                // Fire off all of the MQTT clients
                LaunchMQTTClients(sourceConfigs);
                
                // Run forever, or until receiving a CTRL-C/SIGTERM configured above
                DateTime applicationStarted = DateTime.UtcNow;
                log.Info("Application is up and running!");
                while (true)
                {
                    // Show heartbeat log message every ten seconds, so we know stuff is going on
                    Thread.Sleep(10000);
                    DateTime rightNow = DateTime.UtcNow;
                    TimeSpan timeElapsed = rightNow.Subtract(applicationStarted);
                    log.Info("Uptime: " + timeElapsed.ToString("g"));
                }
                
            }
            catch (Exception ex)
            {
                log.Error("Main application exception: ");
                log.Error(ex);
            }
            
        }
        
        // Handle unhandled.  Primarily for threads
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // Write the exception for later triage
            Console.WriteLine("Unhandled exception:" + (e.ExceptionObject as Exception).ToString());
        }

        // Configures logging with local log4net.config xml file
        private static void ConfigureLogging()
        {
            // This should be the application's only console statement, use log afterwards
            Console.WriteLine("Initializing logging...");

            // Log based on the log4net.config file here, which is console logging
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            log.Info("Logging initalized");
        }

        // Gets the application configuration
        private static AppConfig GetConfiguration(string[] args)
        {
            log.Info("Initializing application configuration");

            // Default is for local flask mock service
            String urlString = "https://localhost:5000/config";

            // Grab the config URL from first argument
            if (args.Length == 1)
            {
                urlString = args[0];
            }

            // Try forever to get application configuration
            AppConfig appConfig = null;
            while (appConfig == null)
            {
                appConfig = GetConfigRequest(urlString).Result;

                // Little sleep before trying again
                if (appConfig == null)
                {
                    log.Info("Sleeping fifteen seconds before trying to configure again");
                    Thread.Sleep(15000);
                }
            }

            log.Info("Application configuration received");
            return appConfig;
        }

        // Creates the individual source configs needed by the MQTT clients, from the main application config
        private static List<MqttConfig> CreateSourceConfigs()
        {
            List<MqttConfig> retval = new List<MqttConfig>();

            foreach (MqttTopicConfig topic in _appConfig.AppSourceConfig.MqttTopics)
            {
                retval.Add(new MqttConfig()
                {
                        Host = _appConfig.AppSourceConfig.Host,
                        Port = _appConfig.AppSourceConfig.Port,
                        Password = _appConfig.AppSourceConfig.Password,
                        UserName = topic.User,
                        Topic = topic.Topic,
                        ReconnectSeconds = _appConfig.AppSourceConfig.DroppedConnectionRetrySeconds,
                        SecureEnabled = _appConfig.AppSourceConfig.SecureEnabled
                    }
                );
            }

            return retval;
        }

        // Handle message arriving: Parse raw message, prepare to send, and send it
        private static void Client_MessageArrived(object sender, MessageEventArgs e)
        {
            try
            {
                if (e.Message == null || e.Message.Length == 0)
                    return;


                // Drop messages we are not going to ingest for now
                string lowercaseTopic = e.Topic.ToLower();
                if (lowercaseTopic.Contains("preservation") || lowercaseTopic.Contains("psychrometric") || lowercaseTopic.Contains("hvac") || lowercaseTopic.Contains("air_flow"))
                {
                    return;
                }
                
                JObject rawMessage = JObject.Parse(Encoding.UTF8.GetString(e.Message));
                MessageEnvelope<JObject> preparedMessage = PrepareMessage(rawMessage, e.Topic);
                //log.Debug(preparedMessage.Message.ToString(Formatting.None));
                _publishers[(MqttClient)sender].Publish<JObject>(preparedMessage);
                
            }
            catch (Exception ex)
            {   
                log.Error(ex);
            }
        }

        // Stuffs the raw message and the extra fields we need to deliver into a MessageEnvelope
        private static MessageEnvelope<JObject> PrepareMessage(JObject rawMessage, String topicName)
        {
            if (rawMessage == null)
                return null;

            JObject preparedMessage = new JObject();
            preparedMessage["msgId"] = Guid.NewGuid();
            preparedMessage["tenantId"] = _appConfig.AppSourceConfig?.TenantId;
            preparedMessage["mqttTopic"] = topicName;
            preparedMessage["rawMsg"] = rawMessage;

            return new MessageEnvelope<JObject>(preparedMessage, _appConfig.AppDestinationConfig?.RoutingKey);
        }

        // Tries to get the application configuration, returns null if unable
        public static async Task<AppConfig> GetConfigRequest(String configUrl)
        {
            AppConfig retval = null;
            using (var httpClientHandler = new HttpClientHandler())
            {
                // TODO this must be removed before a real production release.  This will allow the certificate-less flask mock-service
                // to be usable for now.   Implication is we need to ensure the real configuration endpoint has valid SSL certificates for HTTPS
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                using (HttpClient client = new HttpClient(httpClientHandler))
                {
                    String responseBody = null;
                    try
                    {
                        // Grab the response, make sure it is a success return code before parsing further
                        log.Info("Sending request for application configuration to " + configUrl);
                        HttpResponseMessage response = await client.GetAsync(configUrl);
                        response.EnsureSuccessStatusCode();
                        responseBody = await response.Content.ReadAsStringAsync();

                        // Do some basic validation and null it out if not passing
                        AppConfig unvalidated = ParseAppConfig(responseBody);
                        if (ValidateAppConfig(unvalidated))
                        {
                            retval = unvalidated;
                        }
                    }
                    catch (HttpRequestException ex)
                    {
                        log.Error("Failed to get config from service", ex);
                    }
                }
            }
            return retval;
        }

        // Deserializes appconfig from response text.
        public static AppConfig ParseAppConfig(string responseContent)
        {
            AppConfig retval = null;
            try
            {
                retval = JsonConvert.DeserializeObject<AppConfig>(responseContent);
            }
            catch (JsonSerializationException ex)
            {
                log.Error("Failed to JSON parse config with input string: " + responseContent, ex);
                return null;
            }
            return retval;
        }

        // Validate the application config is correct, both in serialization and content.
        // Returns a validated AppConfig object if validated, else null
        public static bool ValidateAppConfig(AppConfig appConfig)
        {
            // Ensure we had parsed both a source and destination config
            if (appConfig?.AppSourceConfig == null || appConfig?.AppDestinationConfig == null)
            {
                log.Error("Source and/or destination info not present");
                return false;
            }

            // Ensure we were not given an invalid exchange type
            if (appConfig?.AppDestinationConfig.ExchangeType != null && ! Enum.IsDefined(typeof(RabbitMQConfig.AllowableExchangeTypes), appConfig.AppDestinationConfig.ExchangeType))
            {
                log.Error("Unknown exchange type " + appConfig.AppDestinationConfig.ExchangeType);
                return false;
            }

            return true;
        }

        // Establish MQTT clients and launch them
        private static void LaunchMQTTClients(List<MqttConfig> sourceConfigs)
        { 
            foreach (MqttConfig config in sourceConfigs)
            {
                ExchangePublisherOptions publisherOptions = new ExchangePublisherOptions();
                publisherOptions.Name = _appConfig.AppDestinationConfig.Exchange;
                publisherOptions.Type = _appConfig.AppDestinationConfig.ExchangeType;
                
                Publisher publisher = new Publisher(_rabbitConnection, publisherOptions);
                MqttClient client = new MqttClient(config);
                _publishers[client] = publisher;
                client.MessageArrived += Client_MessageArrived;
                client.Run();
                _clients.Add(client);
            }
        }
        
        // Response for a health check request
        private static HealthCheckResult Listener_HealthRequested()
        {
            JObject detailsJson = new JObject();
            JArray ingesterFeeds = new JArray();

            // Tracking unhealthy state
            bool healthy = true;
            
            foreach (MqttClient client in _clients)
            {
                JObject feedInfo = new JObject();
                
                // MQTT stuff
                feedInfo["mqtt_topic"] = client.Config.Topic;
                feedInfo["mqtt_messages_received"] = client.MessageReceivedCount;
                feedInfo["mqtt_last_message_received"] = client.LastMessageReceivedTime;
                feedInfo["mqtt_disconnects"] = client.DisconnectsCount;
                feedInfo["mqtt_last_disconnect"] = client.LastDisconnectTime;

                // Any disconnected clients (that are not initializing), indicate unhealthy app
                if (client.ClientConnectionState == ClientState.Disconnected)
                {
                    healthy = false;
                }

                // Rabbit stuff
                feedInfo["rabbit_messages_published"] = _publishers[client].Count;
                feedInfo["rabbit_last_message_published"] = _publishers[client].LastPublishedTime;
                ingesterFeeds.Add(feedInfo);
            }
            detailsJson["ingester_feeds"] = ingesterFeeds;

            return new HealthCheckResult
            {
                Healthy = healthy,
                ApplicationName = "Ingester",
                Details = detailsJson,
                Message = _appConfig == null ? "Awaiting configuration" : "Running",
                Version = Assembly.GetExecutingAssembly().GetName().Version.ToString()
            };
        }

        // Graceful shutdown
        private static void AppShutdown()
        {
            // Shutdown Mqttclients and their publishers
            log.Info("Shutting down MQTT clients and Rabbit publishers");
            foreach (MqttClient client in _clients)
            {
                try
                {
                    client.Stop();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }

                _publishers[client].TryDispose();
            }
            
            // Close the rabbit MQ connection
            log.Info("Shutting down connection to RabbitMQ server");
            try
            {
                _rabbitConnection.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            // Shutdown the healthcheck listener
            log.Info("Shutting down health check listener");
            try
            {
                _healthCheckListener.Stop();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            log.Info("Application has shut down");
        }
    }
}