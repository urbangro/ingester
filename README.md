# Ingester application
This application is responsible for collecting readings from a remote client,
and routing them to a queue for further processing.  It is multithreaded, with
one thread for each client listening for data to arrive over an MQTT connection,
and then sending records as they arrive to a RabbitMQ queue defined by the config.

A mock configuration service can be found under ./mock/config_service.py

## Docker-in-docker
When running the functional tests from within a docker container, the host level
docker sock must be made available to the running container, and the container must
have docker library installed to have access to the API calls.  See target run-tests

## Project Structure
 - mock - Contains definition for a mock configuration service
 - test - Contains application functional test
 - UrbanGro.Ingester - Main application source
 - UrbanGro.Ingester.Tests - Unit tests








