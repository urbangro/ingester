
## Ingester release notes
 - The versions listed below are not yet linked with the automatic Azure versioning, 
   so numbers below may not appear in Azure, and vice versa.

# 1.5.0
 - Health check API added that listens on <all IPs>:8000/health
 - Private Nuget repository changed to Azure

# 1.4.0
 - MQTT client can connect in secure and unsecure mode, so we can use the local DEV MQTT broker

# 1.3.0
 - Hvac and airflow readings are dropped

# 1.2.0
 - Destination in mock config service can be overridden with an argument
 - Functional and negative unit tests added

# 1.1.0
 - MQTT and Rabbit clients - implemented robustness to handle network disconnects

# 1.0.0
 - Basic project structure established
 - Software now builds as a docker image
 - Log4net logging library incorporated
 - Application self-configures with an HTTP interface application argument
 - Both CTRL-C and SIGTERM will shutdown the application
 - Preservation and psychrometric readings are dropped