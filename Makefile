
.DEFAULT_GOAL := dist

APP_NAME = ingester
BUILD_DIR = build
BUILD_CONTEXT = $(BUILD_DIR)/build_context
DIST_CONTEXT  = $(BUILD_DIR)/dist_context
UNITTEST_CONTEXT = $(BUILD_DIR)/unittest_context

# This will build the ingester software in an SDK image
build: clean

	# Pull the SDK image, so we have latest
	docker pull microsoft/dotnet:2.2-sdk-alpine3.8

	# Establish build context
	mkdir -p $(BUILD_CONTEXT)
	
	# Copy all build artifacts to context, excluding local dev stuff
	cp UrbanGro.Ingester.sln $(BUILD_CONTEXT)
	cp installcredprovider.sh $(BUILD_CONTEXT)
	rsync -av UrbanGro.Ingester       $(BUILD_CONTEXT) --exclude .gitignore --exclude .vs --exclude bin --exclude obj
	rsync -av UrbanGro.Ingester.Tests $(BUILD_CONTEXT) --exclude .gitignore --exclude .vs --exclude bin --exclude obj
		
	# Build it
	docker build --file Dockerfile.build --tag $(APP_NAME)_build $(BUILD_CONTEXT)
	
	
# This will package the ingester software into a releasable image (runtime artifacts)
dist: build

	# Pull the .NET runtime image, so we have latest
	docker pull microsoft/dotnet:2.2-runtime-alpine3.8
	
	# Establish dist context
	mkdir -p $(DIST_CONTEXT)
	
	# Launch an immediately-exiting container of the build, so we can use docker cp.
	# This is a single line make command so that set can save the container ID
	set -e; \
	CONTAINER_ID=$$(docker run -d $(APP_NAME)_build echo "Docker cp requires a container"); \
	docker cp $$CONTAINER_ID:/src/UrbanGro.Ingester/bin $(DIST_CONTEXT); \
	
	# Release notes
	cp RELEASE.md $(DIST_CONTEXT)
	
	# Build it
	docker build --file Dockerfile.dist --tag $(APP_NAME)_dist $(DIST_CONTEXT)

# This will construct a runnable mock-service container, and the test container
build-test-images:

	# Pull python image, so we have latest
	docker pull python:3.7-alpine3.8
	
	# Build mock service and test images
	docker build --tag $(APP_NAME)_mock_service mock
	docker build --tag $(APP_NAME)_test test

# This will run the functional tests which generally validate the application
run-tests:

	docker run -ti --network="host" -v /var/run/docker.sock:/var/run/docker.sock $(APP_NAME)_test

# This will run the unit tests
run-unit-tests:

	docker run -ti $(APP_NAME)_build dotnet test
	
# Convenience target to build and run all tests, quick way to validate.
# Consider running `docker rmi --force $(docker images -a -q)` before running this to truly validate it works from nothing
build-and-run-tests: dist build-test-images run-unit-tests run-tests

	echo "Ingester built and tested successfully!"

# Creates the rabbitMQ test user, needed by the tests
create-rabbit-test-user:

	sudo rabbitmqctl add_user test test
	sudo rabbitmqctl set_user_tags test administrator
	sudo rabbitmqctl set_permissions -p / test ".*" ".*" ".*"

# Just clears the build directory
clean:

	rm -rf $(BUILD_DIR)

	
	